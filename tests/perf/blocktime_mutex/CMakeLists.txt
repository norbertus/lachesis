# Project details
project(blocktime_mutex)

# Header and source files
set (${PROJECT_NAME}_headers "")
set (${PROJECT_NAME}_sources "blocktime_mutex.c")

# Create the library
add_executable (${PROJECT_NAME} ${${PROJECT_NAME}_headers}
				${${PROJECT_NAME}_sources})

# Linker
target_link_libraries(${PROJECT_NAME} realtime pthread rt m)

install(TARGETS ${PROJECT_NAME}	RUNTIME DESTINATION bin)

# Xenomai project deatils
if( XENOMAI_FOUND )

	project( xeno_blocktime_mutex )

	# Header and source files
	set (${PROJECT_NAME}_headers "")
	set (${PROJECT_NAME}_sources "blocktime_mutex.c")

	# Create the library
	add_executable (${PROJECT_NAME} ${${PROJECT_NAME}_headers}
					${${PROJECT_NAME}_sources})

	set_target_properties( xeno_blocktime_mutex PROPERTIES COMPILE_FLAGS "-DKERN_XENOMAI ${XENOMAI_DEFINITIONS}")

	# Linker
	target_link_libraries(${PROJECT_NAME} xeno_realtime m)

	install(TARGETS ${PROJECT_NAME}	RUNTIME DESTINATION bin)

endif( XENOMAI_FOUND )

# RTAI project deatils
if( RTAI_FOUND )

	project( rtai_blocktime_mutex )

	# Header and source files
	set (${PROJECT_NAME}_headers "")
	set (${PROJECT_NAME}_sources "blocktime_mutex.c")

	# Create the library
	add_executable (${PROJECT_NAME} ${${PROJECT_NAME}_headers}
					${${PROJECT_NAME}_sources})

	set_target_properties( rtai_blocktime_mutex PROPERTIES COMPILE_FLAGS "-DKERN_RTAI ${RTAI_DEFINITIONS}")

	# Linker
	target_link_libraries(${PROJECT_NAME} rtai_realtime m)

	install(TARGETS ${PROJECT_NAME}	RUNTIME DESTINATION bin)

endif( RTAI_FOUND )