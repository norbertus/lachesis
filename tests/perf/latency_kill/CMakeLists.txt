# Project details
project(latency_kill)

# Header and source files
set (${PROJECT_NAME}_headers "")
set (${PROJECT_NAME}_sources "latency_kill.c")

# Create the library
add_executable (${PROJECT_NAME} ${${PROJECT_NAME}_headers}
				${${PROJECT_NAME}_sources})

# Linker
target_link_libraries(${PROJECT_NAME} realtime pthread rt m)

install(TARGETS ${PROJECT_NAME}	RUNTIME DESTINATION bin)

# Xenomai project deatils
if( XENOMAI_FOUND )

	project( xeno_latency_kill )

	# Header and source files
	set (${PROJECT_NAME}_headers "")
	set (${PROJECT_NAME}_sources "latency_kill.c")

	# Create the library
	add_executable (${PROJECT_NAME} ${${PROJECT_NAME}_headers}
					${${PROJECT_NAME}_sources})

	set_target_properties( xeno_latency_kill PROPERTIES COMPILE_FLAGS "-DKERN_XENOMAI ${XENOMAI_DEFINITIONS}")

	# Linker
	target_link_libraries(${PROJECT_NAME} xeno_realtime m)

	install(TARGETS ${PROJECT_NAME}	RUNTIME DESTINATION bin)

endif( XENOMAI_FOUND )

# RTAI project deatils
if( RTAI_FOUND )

	project( rtai_latency_kill )

	# Header and source files
	set (${PROJECT_NAME}_headers "")
	set (${PROJECT_NAME}_sources "latency_kill.c")

	# Create the library
	add_executable (${PROJECT_NAME} ${${PROJECT_NAME}_headers}
					${${PROJECT_NAME}_sources})

	set_target_properties( rtai_latency_kill PROPERTIES COMPILE_FLAGS "-DKERN_RTAI ${RTAI_DEFINITIONS}")

	# Linker
	target_link_libraries(${PROJECT_NAME} rtai_realtime m)

	install(TARGETS ${PROJECT_NAME}	RUNTIME DESTINATION bin)

endif( RTAI_FOUND )