/******************************************************************************
 *
 *   Copyright © International Business Machines  Corp., 2006-2008
 *
 *   This program is free software;  you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY;  without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See
 *   the GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program;  if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 *
 * NAME
 *       librttest.c
 *
 * DESCRIPTION
 *      A set of commonly used convenience functions for writing
 *      threaded realtime test cases.
 *
 * USAGE:
 *       To be included in testcases.
 *
 * AUTHOR
 *	Darren Hart <dvhltc@us.ibm.com>
 *
 * HISTORY
 *      2006-Apr-26: Initial version by Darren Hart
 *      2006-May-08: Added atomic_{inc,set,get}, thread struct, debug function,
 *		      rt_init, buffered printing -- Vernon Mauery
 *      2006-May-09: improved command line argument handling
 *      2007-Jul-12: Added latency tracing functions and I/O helper functions
 *					      -- Josh triplett
 *	2008-Jan-10: Added RR thread support to tests -- Chirag Jog
 *
 *****************************************************************************/

#include <librttest.h>
#include <libstats.h>

#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <string.h>
#include <pthread.h>
#include <sched.h>
#include <errno.h>
#include <unistd.h>
#include <getopt.h>
#include <sys/prctl.h>
#include <sys/stat.h>
#include <sys/syscall.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <math.h>

static LIST_HEAD ( taskset );
static atomic_t task_count = {-1};

mutex _buffer_mutex;
char * _print_buffer = NULL;
int _print_buffer_offset = 0;
int _dbg_lvl = 0;
double pass_criteria;

static int _use_pi = 1;

/* function implementations */
void rt_help(void)
{
	printf("librt standard options:\n");
	printf("  -b(0,1)	1:enable buffered output, 0:disable buffered output\n");
	printf("  -p(0,1)	0:don't use pi mutexes, 1:use pi mutexes\n");
	printf("  -v[0-4]	0:no debug, 1:DBG_ERR, 2:DBG_WARN, 3:DBG_INFO, 4:DBG_DEBUG\n");
	printf("  -s		Enable saving stats data (default disabled)\n");
	printf("  -c		Set pass criteria\n");
}

/* Calibrate the busy work loop */
void calibrate_busyloop(void)
{
	volatile unsigned long long int i = CALIBRATE_LOOPS;
	volatile RTIME start, end;

	// The cicle should not be preempted!
	struct sched_param sp;
	sp.sched_priority = 99;
	sched_setscheduler ( 0, SCHED_FIFO, &sp );

	start = rt_gettime();
	while (--i > 0) {
		continue;
	}
	end = rt_gettime();

	iters_per_us = (CALIBRATE_LOOPS * NS_PER_US) / (end-start);
#if DEBUG
	printf ("iters_per_us: %d", iters_per_us);
#endif
}

int rt_init_long(const char *options, const struct option *longopts,
		 int (*parse_arg)(int option, char *value), int argc,
		 char *argv[])
{
	const struct option *cur_opt;
	int use_buffer = 1;
	char *longopt_vals;
	size_t i;
	int c;
	opterr = 0;
	char *all_options;

	if (asprintf(&all_options, ":b:mp:v:sc:%s", options) == -1) {
		fprintf(stderr, "Failed to allocate string for option string\n");
		exit(1);
	}

	/* Check for duplicate options in optstring */
	for (i=0; i<strlen(all_options); i++) {
		char opt = all_options[i];

		if (opt == ':')
			continue;

		/* Search ahead */
		if (strchr(&all_options[i+1], opt)) {
			fprintf(stderr, "Programmer error -- argument -%c already used at least twice\n", opt);
			exit(1);
		}
	}

	/* Ensure each long options has a known unique short option in val. */
	longopt_vals = "";
	cur_opt = longopts;
	while (cur_opt && cur_opt->name) {
		if (cur_opt->flag) {
			fprintf(stderr, "Programmer error -- argument --%s flag"
				" is non-null\n", cur_opt->name);
			exit(1);
		}
		if (!strchr(all_options, cur_opt->val)) {
			fprintf(stderr, "Programmer error -- argument --%s "
				"shortopt -%c wasn't listed in options (%s)\n",
				cur_opt->name, cur_opt->val, all_options);
			exit(1);
		}
		if (strchr(longopt_vals, cur_opt->val)) {
			fprintf(stderr, "Programmer error -- argument --%s "
				"shortopt -%c is used more than once\n",
				cur_opt->name, cur_opt->val);
			exit(1);
		}
		if (asprintf(&longopt_vals, "%s%c", longopt_vals, cur_opt->val) < 0) {
			perror("asprintf");
			exit(2);
		}
		cur_opt++;
	}

	while ((c = getopt_long(argc, argv, all_options, longopts, NULL)) != -1) {
		switch (c) {
		case 'c':
			pass_criteria = atof(optarg);
			break;
		case 'b':
			use_buffer = atoi(optarg);
			break;
		case 'p':
			_use_pi = atoi(optarg);
			break;
		case 'v':
			_dbg_lvl = atoi(optarg);
			break;
		case 's':
			save_stats = 1;
			break;
		case ':':
			if (optopt == '-')
				fprintf(stderr, "long option missing arg\n");
			else
				fprintf(stderr, "option -%c: missing arg\n", optopt);
			parse_arg('h', optarg); /* Just to display usage */
			exit (1); /* Just in case. (should normally be done by usage()) */
		case '?':
			if (optopt == '-')
				fprintf(stderr, "unrecognized long option\n");
			else
				fprintf(stderr, "option -%c not recognized\n", optopt);
			parse_arg('h', optarg); /* Just to display usage */
			exit (1); /* Just in case. (should normally be done by usage()) */
		default:
			if (parse_arg && parse_arg(c, optarg))
				break; /* Application option */

			fprintf(stderr, "Programmer error -- option -%c defined but not handled\n", c);
			exit(1);
		}
	}
	if (!_use_pi)
		printf("Priority Inheritance has been disabled for this run.\n");
	if (use_buffer)
		buffer_init();

	// RTAI and Xenomai require this - to be fair we impose this to all tests
	mlockall(MCL_CURRENT|MCL_FUTURE);

	// To calibrate busy_loop in PREEMPT_RT mode.
	calibrate_busyloop();

	/*
	 * atexit() order matters here - buffer_print() will be called before
	 * buffer_fini().
	 */
	atexit(buffer_fini);
	atexit(buffer_print);
	return 0;
}

int rt_init(const char *options, int (*parse_arg)(int option, char *value),
	    int argc, char *argv[])
{
	return rt_init_long(options, NULL, parse_arg, argc, argv);
}

void buffer_init(void)
{
	_print_buffer = (char *)malloc(PRINT_BUFFER_SIZE);
	if (!_print_buffer)
		fprintf(stderr, "insufficient memory for print buffer - printing directly to stderr\n");
	else
		memset(_print_buffer, 0, PRINT_BUFFER_SIZE);
}

void buffer_print(void)
{
	if (_print_buffer) {
		fprintf(stderr, "%s", _print_buffer);
		memset(_print_buffer, 0, PRINT_BUFFER_SIZE);
		_print_buffer_offset = 0;
	}
}

void buffer_fini(void)
{
	if (_print_buffer)
		free(_print_buffer);
	_print_buffer = NULL;
}

void cleanup(int i) {
	printf("Test terminated with asynchronous signal\n");
	buffer_print();
	buffer_fini();
	if (i)
		exit (i);
}

void setup()
{
	signal(SIGINT, cleanup);
	signal(SIGQUIT,cleanup);
	signal(SIGTERM, cleanup);
}

void ts_minus(struct timespec *ts_end, struct timespec *ts_start, struct timespec *ts_delta)
{
	if (ts_end == NULL || ts_start == NULL || ts_delta == NULL) {
		printf("ERROR in %s: one or more of the timespecs is NULL", __FUNCTION__);
		return;
	}

	ts_delta->tv_sec = ts_end->tv_sec - ts_start->tv_sec;
	ts_delta->tv_nsec = ts_end->tv_nsec - ts_start->tv_nsec;
	ts_normalize(ts_delta);
}

void ts_plus(struct timespec *ts_a, struct timespec *ts_b, struct timespec *ts_sum)
{
	if (ts_a == NULL || ts_b == NULL || ts_sum == NULL) {
		printf("ERROR in %s: one or more of the timespecs is NULL", __FUNCTION__);
		return;
	}

	ts_sum->tv_sec = ts_a->tv_sec + ts_b->tv_sec;
	ts_sum->tv_nsec = ts_a->tv_nsec + ts_b->tv_nsec;
	ts_normalize(ts_sum);
}

void ts_normalize(struct timespec *ts)
{
	if (ts == NULL) {
		/* FIXME: write a real error logging system */
		printf("ERROR in %s: ts is NULL\n", __FUNCTION__);
		return;
	}

	/* get the abs(nsec) < NS_PER_SEC */
	while (ts->tv_nsec > NS_PER_SEC) {
		ts->tv_sec++;
		ts->tv_nsec -= NS_PER_SEC;
	}
	while (ts->tv_nsec < -NS_PER_SEC) {
		ts->tv_sec--;
		ts->tv_nsec += NS_PER_SEC;
	}

	/* get the values to the same polarity */
	if (ts->tv_sec > 0 && ts->tv_nsec < 0) {
		ts->tv_sec--;
		ts->tv_nsec += NS_PER_SEC;
	}
	if (ts->tv_sec < 0 && ts->tv_nsec > 0) {
		ts->tv_sec++;
		ts->tv_nsec -= NS_PER_SEC;
	}
}

int ts_to_nsec(struct timespec *ts, RTIME *ns)
{
	struct timespec t;
	if (ts == NULL) {
		/* FIXME: write a real error logging system */
		printf("ERROR in %s: ts is NULL\n", __FUNCTION__);
		return -1;
	}
	t.tv_sec = ts->tv_sec;
	t.tv_nsec = ts->tv_nsec;
	ts_normalize(&t);

	if (t.tv_sec <= 0 && t.tv_nsec < 0) {
		printf("ERROR in %s: ts is negative\n", __FUNCTION__);
		return -1;
	}

	*ns = (RTIME)ts->tv_sec*NS_PER_SEC + ts->tv_nsec;
	return 0;
}

void nsec_to_ts(RTIME ns, struct timespec *ts)
{
	if (ts == NULL) {
		/* FIXME: write a real error logging system */
		printf("ERROR in %s: ts is NULL\n", __FUNCTION__);
		return;
	}
	ts->tv_sec = ns/NS_PER_SEC;
	ts->tv_nsec = ns%NS_PER_SEC;
}

/* return difference in microseconds */
RTIME rt_minus(RTIME start, RTIME end)
{
	RTIME delta;
	if (start <= end)
		delta = end - start;
	else {
		delta = ULL_MAX - (end - start) + 1;
		printf("TSC wrapped, delta=%llu\n", delta);
	}
	return delta;
}

RTIME rt_gettime(void) {
#if KERN_RTAI
	return rt_get_time_ns();
#elif KERN_XENOMAI
	return rt_timer_read();
#else
	struct timespec ts;
	RTIME ns;
	int rc;

	rc = clock_gettime(CLOCK_MONOTONIC, &ts);
	if (rc != 0) {
		printf("ERROR in %s: clock_gettime() returned %d\n", __FUNCTION__, rc);
		perror("clock_gettime() failed");
		return 0;
	}

	ts_to_nsec(&ts, &ns);
	return ns;
#endif
}

/**
 * busy wait for ns nanosecond
 */
void rt_busywork(RTIME ns)
{
#if KERN_RTAI
	rt_busy_sleep(ns);
#elif KERN_XENOMAI
	rt_timer_spin(ns);
#else
	volatile unsigned long long int i;
	RTIME start, now;
	int delta; /* time in us */

	i = (ns * iters_per_us) / NS_PER_US;

	start = rt_gettime();
	while (--i > 0) {
		continue;
	}
	now = rt_gettime();

	delta = (now - start)/NS_PER_US;
	/* uncomment to tune to your machine */
	/* printf("busy_work_us requested: %dus  actual: %dus\n", us, delta); */
#endif
}

/**
* sleep for ns nanosecond
*/
void rt_nanosleep(RTIME ns)
{
#if KERN_RTAI
	rt_sleep(nano2count(ns));
#elif KERN_XENOMAI
	rt_task_sleep((RTIME)ns);
#else
	struct timespec ts_sleep, ts_rem;
	int rc;
	nsec_to_ts(ns, &ts_sleep);
	rc = clock_nanosleep(CLOCK_MONOTONIC, 0, &ts_sleep, &ts_rem);
	/* FIXME: when should we display the remainder ? */
	if (rc != 0) {
		printf("WARNING: rt_nanosleep() returned early by %d s %d ns\n",
		       (int)ts_rem.tv_sec, (int)ts_rem.tv_nsec);
	}
#endif
}

/**
* sleep for ns nanosecond, then wake
*/
void rt_nanosleep_until(RTIME ns)
{
#if KERN_RTAI
	rt_sleep_until(nano2count(ns));
#elif KERN_XENOMAI
	rt_task_sleep_until(ns);
#else
	struct timespec ts_sleep, ts_rem;
	int rc;
	nsec_to_ts(ns, &ts_sleep);
	rc = clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &ts_sleep, &ts_rem);
	/* FIXME: when should we display the remainder ? */
	if (rc != 0) {
		printf("WARNING: rt_nanosleep() returned early by %d s %d ns\n",
		       (int)ts_rem.tv_sec, (int)ts_rem.tv_nsec);
	}
#endif
}

/* Write the entirety of data.  Complain if unable to do so. */
static void write_or_complain(int fd, const void *data, size_t len)
{
	const char *remaining = data;

	while (len > 0) {
		ssize_t ret = write(fd, remaining, len);
		if (ret <= 0) {
			if (errno != EAGAIN && errno != EINTR) {
				perror("write");
				return;
			}
		} else {
			remaining += ret;
			len -= ret;
		}
	}
}

/* Write the given data to the existing file specified by pathname.  Complain
 * if unable to do so. */
static void write_file(const char *pathname, const void *data, size_t len)
{
	int fd = open(pathname, O_WRONLY);
	if (fd < 0) {
		printf("Failed to open file \"%s\": %d (%s)\n",
		       pathname, errno, strerror(errno));
		return;
	}

	write_or_complain(fd, data, len);

	if (close(fd) < 0) {
		printf("Failed to close file \"%s\": %d (%s)\n",
		       pathname, errno, strerror(errno));
	}
}

/* Write the given '\0'-terminated string to the existing file specified by
 * pathname.  Complain if unable to do so. */
static void write_string_to_file(const char *pathname, const char *string)
{
	write_file(pathname, string, strlen(string));
}

static void read_and_print(const char *pathname, int output_fd)
{
	char data[4096];
	int fd = open(pathname, O_RDONLY);
	if (fd < 0) {
		printf("Failed to open file \"%s\": %d (%s)\n",
		       pathname, errno, strerror(errno));
		return;
	}

	while (1) {
		ssize_t ret = read(fd, data, sizeof(data));
		if (ret < 0) {
			if (errno != EAGAIN && errno != EINTR) {
				printf("Failed to read from file \"%s\": %d (%s)\n",
					pathname, errno, strerror(errno));
				break;
			}
		} else if (ret == 0)
			break;
		else
			write_or_complain(output_fd, data, ret);
	}

	if (close(fd) < 0) {
		printf("Failed to close file \"%s\": %d (%s)\n",
			pathname, errno, strerror(errno));
	}
}

void latency_trace_enable(void)
{
	printf("Enabling latency tracer.\n");
	write_string_to_file("/proc/sys/kernel/trace_use_raw_cycles", "1");
	write_string_to_file("/proc/sys/kernel/trace_all_cpus", "1");
	write_string_to_file("/proc/sys/kernel/trace_enabled", "1");
	write_string_to_file("/proc/sys/kernel/trace_freerunning", "1");
	write_string_to_file("/proc/sys/kernel/trace_print_on_crash", "0");
	write_string_to_file("/proc/sys/kernel/trace_user_triggered", "1");
	write_string_to_file("/proc/sys/kernel/trace_user_trigger_irq", "-1");
	write_string_to_file("/proc/sys/kernel/trace_verbose", "0");
	write_string_to_file("/proc/sys/kernel/preempt_thresh", "0");
	write_string_to_file("/proc/sys/kernel/wakeup_timing", "0");
	write_string_to_file("/proc/sys/kernel/mcount_enabled", "1");
	write_string_to_file("/proc/sys/kernel/preempt_max_latency", "0");
}

#ifndef PR_SET_TRACING
#define PR_SET_TRACING 0
#endif

void latency_trace_start(void)
{
	if (prctl(PR_SET_TRACING, 1) < 0)
		perror("Failed to start tracing");
}

void latency_trace_stop(void)
{
	if (prctl(PR_SET_TRACING, 0) < 0)
		perror("Failed to stop tracing");
}

void latency_trace_print(void)
{
	read_and_print("/proc/latency_trace", STDOUT_FILENO);
}

/*--- Task management functions ---*/

task taskset_init ( int num )
{
	task info;
	int i;

	info = calloc ( num, sizeof ( struct task_t ) );
	if ( !info ) {
		printf ( "Insufficient memory initializing task set\n" );
		exit ( 1 );
	}
	for ( i=0; i<num; i++ ) {
		info[i].prio=1;
		info[i].policy=SCHED_FIFO;
		info[i].cpus_allowed = 0xFF;
	}
	return info;
}

int task_create ( task t )
{
	int id, ret;

	id = atomic_inc ( &task_count );
	list_add_tail ( &t->list, &taskset );
	t->id = id;
	
#if KERN_RTAI
	t->desc = rt_thread_init ( nam2num ( t->name ), t->prio, t->max_msg_size, t->policy, t->cpus_allowed );
	if ( &( t->desc ) == NULL ) {
		printf ( "[RTAI] Task creation failed" );
		list_del ( &t->list );
		free ( t );
		return -1;
	}
#elif KERN_XENOMAI
	if ( ( ret  = rt_task_create ( t->desc, t->name, t->stack_size, t->prio, t->flags ) ) != 0 ) {
		printf ( "[Xenomai] Task creation failed: %d (%s)\n", ret, strerror ( ret ) );
		list_del ( &t->list );
		free ( t );
		return -1;
	}
#else
	struct sched_param param;
	pthread_cond_init ( &t->cond, NULL );	// Accept the defaults
	mutex_init ( &t->lock );

	param.sched_priority = t->prio;

	pthread_attr_init ( &t->attr );
	pthread_attr_setinheritsched ( &t->attr, PTHREAD_EXPLICIT_SCHED );
	pthread_attr_setschedpolicy ( &t->attr, t->policy );
	pthread_attr_setschedparam ( &t->attr, &param );

	if ( ( ret = pthread_create ( &t->desc, &t->attr, t->func, t->arg ) ) ) {
		printf ( "[POSIX] Task creation failed: %d (%s)\n", ret, strerror ( ret ) );
		list_del ( &t->list );
		pthread_attr_destroy ( &t->attr );
		free ( t );
		return -1;
	}
	pthread_attr_destroy ( &t->attr );
#endif
	return 0;
}

int task_start ( int id )
{
	int ret;
	task t = task_get ( id );
#if KERN_RTAI
	rt_thread_create ( t->func, t->arg, t->stack_size );
#elif KERN_XENOMAI
	if ( ( ret = rt_task_start( t->desc, (void (*)(void*))t->func, t->arg ) ) != 0 )
	{
		printf ( "[Xenomai] Failed to start task: %d (%s)\n", ret, strerror ( ret ) );
		return -1;
	}
#endif
	return 0;
}

int task_set_priority ( int id, int prio )
{
	// Recupera il task indicato da ID, ne setta la priorità ed esce
	task t = task_get ( id );
	int ret = 0;
#if KERN_RTAI
	if ( ( ret = rt_change_prio ( t->desc, prio ) ) != 0 ) {
		printf ( "[RTAI] Failed to set priority: %d (%s)\n", ret, strerror ( ret ) );
		return -1;
	}
#elif KERN_XENOMAI
	if ( ( ret = rt_task_set_priority( t->desc, prio ) ) != 0 ) {
		printf ( "[Xenomai] Failed to set priority: %d (%s)\n", ret, strerror ( ret ) );
		return -1;
	}
#else
	struct sched_param sp;
	sp.sched_priority = prio;

	t->prio = prio;
	// If priority is 0, set automatically policy to SCHED_OTHER
	if ( prio==0 )
		t->policy = SCHED_OTHER;

	if ( sched_setscheduler ( t->desc, t->policy, &sp ) != 0 )
		return -1;
#endif
	t->prio = prio;
	return 0;
}

int task_set_policy ( int id, int policy )
{
	task t = task_get ( id );
	int ret;
#if KERN_RTAI
	if ( policy == SCHED_FIFO )
		rt_set_sched_policy ( t->desc, RT_SCHED_FIFO, 1000 );
	else if ( policy == SCHED_RR ) {
		// TODO Add support for RR quantum directly in task structure
		// Here we have quantum = 1000 just for example		
		rt_set_sched_policy ( t->desc, RT_SCHED_RR, 1000 );
	} else {
		printf ( "[RTAI] Scheduling policy not supported" );
		return -1;
	}
#elif KERN_XENOMAI
	// TODO Add support to change to SCHED_RR specifying the time slice
	printf ( "[Xenomai] Changing scheduling policy is not supported" );
	return -1;
#else
	struct sched_param sp;
	sp.sched_priority = t->prio;
	if ( sched_setscheduler ( t->desc, policy, &sp ) != 0 )
		return -1;
#endif
	t->policy = policy;
	return 0;
}

void task_join ( int i )
{
	task p, t = NULL;
	list_for_each_entry ( p, &taskset, list )
	{
		if ( p->id == i )
		{
			t = p;
			break;
		}
	}
	if ( t )
	{
#if KERN_RTAI
		// WARNING RTAI has not a join primitive.
		// We're going to kill each task, hoping for the best! ;)
		if ( t-> desc )
			rt_task_delete ( t->desc );
#elif KERN_XENOMAI
		if ( t->desc )
			rt_task_join ( t->desc );
#else
		t->flags |= THREAD_QUIT;
		if ( t->desc )
			pthread_join ( t->desc, NULL );
#endif
		list_del ( &t->list );
	}
}

void task_quit_all()
{
	task p;
	list_for_each_entry ( p, &taskset, list )
	{
		p->flags |= THREAD_QUIT;
	}
}

void task_join_all()
{
	task_quit_all();
	task p, t;
	list_for_each_entry_safe ( p, t, &taskset, list )
	{
#if KERN_RTAI
		// WARNING RTAI has not a join primitive.
		// We're going to kill each task, hoping for the best! ;)
		if ( t-> desc )
			rt_task_delete ( t->desc );
#elif KERN_XENOMAI
		if ( p->desc )
			rt_task_join ( p->desc );
#else
		if ( p->desc )
			pthread_join ( p->desc, NULL );
#endif
		list_del ( &p->list );
	}
}

task task_get ( int id )
{
	task t;
	list_for_each_entry ( t, &taskset, list )
	{
		if ( t->id == id ) {
			return t;
		}
	}
	return NULL;
}

void task_inspect ( task t )
{
	printf ( "\nTask n.%d\n", t->id );
	printf ( "Scheduling policy: " );
	if ( t->policy == SCHED_FIFO )
		printf ( "SCHED_FIFO\n" );
	else if ( t->policy == SCHED_RR )
		printf ( "SCHED_RR\n" );
	else
		printf ( "SCHED_OTHER\n" );
	printf ( "Priority: %d\n", t->prio );
	printf ( "Start point at: %p\n", t->func );
	printf ( "Arguments at: %p\n", &t->arg );
	printf ( "Flags: %d\n", t->flags );
	printf ( "Previous task at: %p\n", t->list.prev );
	printf ( "Next task at: %p\n", t->list.next );
}

/*--- Mutex management functions ---*/

int mutex_init ( mutex *m )
{
	int ret;
#if KERN_RTAI
	//if ( ( ret = rt_sem_init ( m, 1 ) ) != 0 )
#elif KERN_XENOMAI
	if ( ( ret = rt_mutex_create ( m, NULL ) ) != 0 )
	{
		printf ( "[Xenomai] Failed to init mutex: %d (%s)\n", ret, strerror ( ret ) );
		return -1;
	}
#else
#if HAS_PRIORITY_INHERIT
	pthread_mutexattr_t attr;
	int protocol;

	if ((ret = pthread_mutexattr_init(&attr)) != 0)
	{
		printf("[POSIX] Failed to init mutexattr: %d (%s)\n", ret, strerror(ret));
		return -1;
	};
	if ((ret = pthread_mutexattr_setprotocol(&attr, PTHREAD_PRIO_INHERIT)) != 0)
	{
		printf("[POSIX] Failed to set mutexattr protocol: %d (%s)\n", ret, strerror(ret));
		return -1;
	}
	if ((ret = pthread_mutexattr_getprotocol(&attr, &protocol)) != 0)
	{
		printf("[POSIX] Failed to get mutexattr protocol: %d (%s)\n", ret, strerror(ret));
		return -1;
	}
	if ((ret = pthread_mutex_init(m, &attr)) != 0)
	{
		printf("[POSIX] Failed to init mutex: %d (%s)\n", ret, strerror(ret));
		return -1;
	}
#endif /* HAS_PRIORITY_INHERIT */
#endif /* KERN_RTAI */
	return 0;
}

int mutex_destroy ( mutex *m )
{
        int ret;
#if KERN_RTAI
#elif KERN_XENOMAI
	if ( ( ret = rt_mutex_delete ( m ) ) != 0 ) {
		printf ( "[Xenomai] Failed to destroy mutex: %d (%s)\n", ret, strerror ( ret ) );
		return -1;
	}
#else
	if ( ( ret = pthread_mutex_destroy ( m ) ) != 0 ) {
		printf ( "[POSIX] Failed to destroy mutex: %d (%s)\n", ret, strerror ( ret ) );
		return -1;
	}
#endif
        return 0;
}

int mutex_lock ( mutex *m )
{
        int ret;
#if KERN_RTAI
#elif KERN_XENOMAI
	if ( ( ret = rt_mutex_acquire ( m, TM_INFINITE ) ) != 0 ) {
		printf ( "[Xenomai] Failed to lock mutex: %d (%s)\n", ret, strerror ( ret ) );
		return -1;
	}
#else
        if ( ( ret = pthread_mutex_lock ( m ) ) != 0 ) {
                printf ( "[POSIX] Failed to lock mutex: %d (%s)\n", ret, strerror ( ret ) );
                return -1;
        }
#endif
        return 0;
}

int mutex_unlock ( mutex *m )
{
	int ret;
#if KERN_RTAI
#elif KERN_XENOMAI
	if ( ( ret = rt_mutex_release ( m ) ) != 0 ) {
		printf ( "[Xenomai] Failed to unlock mutex: %d (%s)\n", ret, strerror ( ret ) );
		return -1;
	}	
#else
	if ((ret = pthread_mutex_unlock(m)) != 0)
	{
		printf("[POSIX] Failed to unlock mutex: %d (%s)\n", ret, strerror(ret));
		return -1;
	}
#endif
	return 0;
}