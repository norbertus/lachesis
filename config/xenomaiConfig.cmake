# (C) Copyright 2005-2007 Johns Hopkins University (JHU), All Rights
# Reserved.
#
# --- begin cisst license - do not edit ---
# 
# This software is provided "as is" under an open source license, with
# no warranty.  The complete license can be found in license.txt and
# http://www.cisst.org/cisst/license.txt.
# 
# --- end cisst license ---

if( UNIX )

	# set the search path
	set( XENOMAI_SEARCH_PATH /usr/local/xenomai /usr/xenomai /usr )
  
	# find xeno-config.h
	find_path( XENOMAI_DIR include/xeno_config.h ${XENOMAI_SEARCH_PATH} )
  
	# did we find xeno_config.h?
	if( XENOMAI_DIR )

		#Xenomai not found
		set ( XENOMAI_FOUND true )
		
		# set the include directory
		if( XENOMAI_DIR STREQUAL "/usr/include/xenomai" )
			# on ubuntu linux
			set( XENOMAI_INCLUDE_DIR ${XENOMAI_DIR} )
			set( XENOMAI_POSIX_INCLUDE_DIR ${XENOMAI_DIR}/posix )
			set( XENOMAI_INCLUDE_DIRS ${XENOMAI_INCLUDE_DIR} ${XENOMAI_POSIX_INCLUDE_DIR})			
		else( XENOMAI_DIR STREQUAL "/usr/include/xenomai")
			# elsewhere
			set( XENOMAI_INCLUDE_DIR ${XENOMAI_DIR}/include )
			set( XENOMAI_POSIX_INCLUDE_DIR ${XENOMAI_DIR}/include/posix )
			set( XENOMAI_INCLUDE_DIRS ${XENOMAI_INCLUDE_DIR} ${XENOMAI_POSIX_INCLUDE_DIR})
		endif( XENOMAI_DIR STREQUAL "/usr/include/xenomai")

		# find the xenomai pthread library
		find_library( XENOMAI_NATIVE_LIBRARY  	native  ${XENOMAI_DIR}/lib )
		find_library( XENOMAI_LIBRARIES 	xenomai ${XENOMAI_DIR}/lib )
		find_library( XENOMAI_PTHREAD_RT_LIBRARY pthread_rt rtdm ${XENOMAI_DIR}/lib )
		find_library( XENOMAI_RTDM_LIBRARY 	rtdm    ${XENOMAI_DIR}/lib )
		# find the posix wrappers
		find_file( XENOMAI_POSIX_WRAPPERS lib/posix.wrappers ${XENOMAI_SEARCH_PATH} )

		# set the linker flags
		set( XENOMAI_RUNTIME_LIBRARY_DIRS "-Wl,@${XENOMAI_POSIX_WRAPPERS}" )

		# add compile/preprocess options
		set( XENOMAI_DEFINITIONS "-D_GNU_SOURCE -D_REENTRANT -Wall -pipe -D__XENO__")

	else ( XENOMAI_DIR )

		#Xenomai not found
		set ( XENOMAI_FOUND false )

	endif( XENOMAI_DIR )

endif( UNIX )
