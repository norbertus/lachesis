# (C) Copyright 2004-2009 Johns Hopkins University (JHU), All Rights
# Reserved.
#
# --- begin cisst license - do not edit ---
# 
# This software is provided "as is" under an open source license, with
# no warranty.  The complete license can be found in license.txt and
# http://www.cisst.org/cisst/license.txt.
# 
# --- end cisst license ---
#
# try to find RTAI on UNIX systems.
#
# The following values are defined
#
# RTAI_INCLUDE_DIR     - include directories to use RTAI
# RTAI_LIBRARIES       - link against this to use RTAI (fullpath)
#

# (C) Copyright 2005-2007 Johns Hopkins University (JHU), All Rights
# Reserved.
#
# --- begin cisst license - do not edit ---
#
# This software is provided "as is" under an open source license, with
# no warranty.  The complete license can be found in license.txt and
# http://www.cisst.org/cisst/license.txt.
#
# --- end cisst license ---

if( UNIX )

	# set the search path
	set( RTAI_SEARCH_PATH /usr/realtime /usr/local /usr )
	
	# find rtai_config.h
	find_path( RTAI_DIR include/rtai_config.h ${RTAI_SEARCH_PATH} )
	
	# did we find rtai_config.h?
	if( RTAI_DIR )

		# RTAI found
		set ( RTAI_FOUND true )

		# set the include directory
		set ( RTAI_INCLUDE_DIRS ${RTAI_DIR}/include )

		# find lxrt library
		find_library( RTAI_LIBRARIES lxrt ${RTAI_DIR}/lib )

 		# set the linker flags
 		set( RTAI_RUNTIME_LIBRARY_DIRS "-Wl,@${RTAI_LIBRARIES}" )
 
 		# add compile/preprocess options
 		set( RTAI_DEFINITIONS "-D_GNU_SOURCE -Wall")

	else ( RTAI_DIR )

		# RTAI non found
		set ( RTAI_FOUND false )

	endif( RTAI_DIR )

endif( UNIX )